/*-----------------------------------------------------------------------------
 * 
 * IDENTIFICATION: idftalgorithm.c
 * Usage: Inverse DFT
 * 
 *-----------------------------------------------------------------------------
 */
#include "dftalgorithm.h"

void inversedft_algortihm(double *_sig_src_arr,
				 double *_sig_src_real_arr,
				 double *_sig_src_imaginary_arr,
				 double *_idft_sig_out,
				 int idft_sig_len)
{
	int i, k;

	for (k = 0; k < idft_sig_len/2; k++)
	{
		_sig_src_real_arr[k] = _sig_src_real_arr[k]/(idft_sig_len/2);
		_sig_src_imaginary_arr[k] = _sig_src_imaginary_arr[k]/(idft_sig_len/2);
	}

	_sig_src_real_arr[0] = _sig_src_arr[0] / 2;
	_sig_src_imaginary_arr[0] = -_sig_src_imaginary_arr[0]/2;

	// Initializing.
	for (i = 0; i < idft_sig_len; i++)
	{
		_idft_sig_out[i] = 0;
	}

	for (k = 0; k < idft_sig_len/2; k++)
	{
		for (i = 0; i < idft_sig_len; i++)
		{
			_idft_sig_out[i] = _idft_sig_out[i] + _sig_src_real_arr[k]*cos((2*PI*k*i)/idft_sig_len);
			_idft_sig_out[i] = _idft_sig_out[i] + _sig_src_imaginary_arr[k]*sin((2*PI*k*i)/idft_sig_len);
		}
	}
}
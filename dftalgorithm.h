/*-----------------------------------------------------------------------------
 * 
 * IDENTIFICATION: dftalgorithm.h
 * 
 *-----------------------------------------------------------------------------
 */
#ifndef DFTALGORITHM_H

#define SIG_LEN	320
#define IMPULSE_RESPONSE_LENGTH 29

#define PI 3.142

// Array of input signal containing 1kHz plus 15kHz.
extern double InputSignal_f32_1kHz_15kHz[SIG_LEN];
// Array of impulse response with cutoff freq. of 6KHz.
extern double  Impulse_response[IMPULSE_RESPONSE_LENGTH];

// DFT Algorithm function
// Takes time-domain signal and produces frequency-domain signal.
extern void dft_algortihm(double *_sig_src_arr,
				 double *_sig_dest_real_arr,
				 double *_sig_dest_imaginary_arr,
				 int _sig_length);

//IDFT Algorithm function.
// Takes frequency-domain signal and produces time-domain signal.
extern void inversedft_algortihm(double *_sig_src_arr,
				 double *_sig_src_real_arr,
				 double *_sig_src_imaginary_arr,
				 double *_idft_sig_out,
				 int idft_sig_len);

#endif /* DFTALGORITHM_H */
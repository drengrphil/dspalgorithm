/*-----------------------------------------------------------------------------
 * 
 * IDENTIFICATION: dftalgorithm.c
 * 
 *-----------------------------------------------------------------------------
 */
#include <math.h> // PI
#include "dftalgorithm.h"

#define SIG_LEN	320


extern double InputSignal_f32_1kHz_15kHz[SIG_LEN];

extern void dft_algortihm(double *_sig_src_arr,
				 double *_sig_dest_real_arr,
				 double *_sig_dest_imaginary_arr,
				 int _sig_length);

// The DFT algorithm takes a time domain signal and produce
// a signal in frequency domain with N/2 real parts and N/2
// imaginary parts.
void dft_algortihm(double *_sig_src_arr,
	double *_sig_dest_real_arr,
	double *_sig_dest_imaginary_arr,
	int _sig_length)
{
	int i, j, k;

	// Initialize.
	for (j = 0; j < _sig_length/2; j++)
	{
		_sig_dest_real_arr[j] = 0;
		_sig_dest_imaginary_arr[j] = 0;
	}

	for (k = 0; k < (_sig_length/2); k++)
	{
		for (i = 0; i < (_sig_length); i++)
		{
			// Real part
			_sig_dest_real_arr[k] = _sig_dest_real_arr[k] + _sig_src_arr[i]*cos((2*PI*k*i)/_sig_length);
			// printf("\n%f", _sig_dest_real_arr[k]);
			// Imaginary part
			_sig_dest_imaginary_arr[k] = _sig_dest_imaginary_arr[k] - _sig_src_arr[i]*sin((2*PI*k*i)/_sig_length);
			// printf("\n%f", _sig_dest_imaginary_arr[k]);
		}
	}
}

// DFT output magnitude calculation
void dft_maginitude_cal()
{
	
}
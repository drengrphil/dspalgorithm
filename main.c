/*-----------------------------------------------------------------------------
 *
 *
 *-----------------------------------------------------------------------------
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h> // pow()
#include "dftalgorithm.h"

// Function to compute the signal mean.
double calc_signal_mean(double *sig_src_arr, int sig_length);
// Function to compute the signal variance.
double calc_signal_variance(double * sig_src_arr, double _sig_mean, int sig_length);
// Function to compute the standard deviation.
double calc_standard_deviation(double _sig_variance);
// FUnction to compute the convolution of two signals
void convolution(double *_sig_src_arr,
		 double *_sig_dest_arr,
		 double *_impulse_resp_arr,
		 int sig_src_len,
		 int imp_resp_len);
// FUnction to calculate running sum.
void running_sum(double *_sig_src_arr,
		 double *_sig_dest_arr,
		 int in_sig_len);
// Function to calculate DFT
// Real parts.
double Output_DFT_Real[SIG_LEN/2];
// Imaginary parts.
double Output_DFT_Imaginary[SIG_LEN/2];

// Mean of the signal.
double _mean_of_signal;
// Variance of the signal.
double _var_of_signal;
// Standard deviation
double _std_of_signal;
// Output signal from convolution algorithm.
double Output_signal_conv[SIG_LEN+IMPULSE_RESPONSE_LENGTH];
// Output signal for running sum.
// Length is the same as input signal length.
double Output_signal_running_sum[SIG_LEN];

// Calculate IDFT
double Output_IDFT_Sig[SIG_LEN];

int main()
{
	// Calculate mean of signal.
	_mean_of_signal = calc_signal_mean(&InputSignal_f32_1kHz_15kHz[0], SIG_LEN);
	// Calculate variance of signal.
	_var_of_signal = calc_signal_variance(&InputSignal_f32_1kHz_15kHz[0], _mean_of_signal, SIG_LEN);
	// Calculate standard deviation of a signal.
	_std_of_signal = calc_standard_deviation(_var_of_signal);

	// File pointers.
	FILE *input_sig_fptr, *imp_resp_fptr, *output_sig_fptr, *output_runsum_fptr;

	input_sig_fptr = fopen("input_sig.dat", "w");
	imp_resp_fptr = fopen("impulse_resp.dat", "w");
	output_sig_fptr = fopen("output_conv_sig.dat", "w");
	output_runsum_fptr = fopen("out_runningsum.dat", "w");

	// Input signals printed to file
	for (int i=0; i < SIG_LEN; i++)
	{
		fprintf(input_sig_fptr, "\n%f", InputSignal_f32_1kHz_15kHz[i]);
	}
	fclose(input_sig_fptr);

	// Impulse response signal printed to file.
	for (int i =0; i < IMPULSE_RESPONSE_LENGTH; ++i)
	{
		fprintf(imp_resp_fptr, "\n%f", Impulse_response[i]);
	}
	fclose(imp_resp_fptr);

	// Convolution
	convolution(&InputSignal_f32_1kHz_15kHz[0],
			 &Output_signal_conv[0],
			 &Impulse_response[0],
			 SIG_LEN,
			 IMPULSE_RESPONSE_LENGTH);

	// Output signal of convolution to file
	for (int i = 0; i < SIG_LEN+IMPULSE_RESPONSE_LENGTH; i++)
	{
		fprintf(output_sig_fptr, "\n%f", Output_signal_conv[i]);
	}
	fclose(output_sig_fptr);

	// Running sum
	running_sum(&InputSignal_f32_1kHz_15kHz[0], &Output_signal_running_sum[0], SIG_LEN);

	// Output signal of running sum
	for (int i = 0; i < SIG_LEN; i++)
	{
		fprintf(output_runsum_fptr, "\n%f", Output_signal_running_sum[i]);
	}
	fclose(output_runsum_fptr);

	// Calculating the DFT of the signal.
	dft_algortihm(&InputSignal_f32_1kHz_15kHz[0],
				 &Output_DFT_Real,
				 &Output_DFT_Imaginary,
				 SIG_LEN);
	// File pointers for storing output.
	FILE *outRe_fptr, *outIm_fptr;

	outRe_fptr = fopen("output_sig_rex.dat", "w");
	outIm_fptr = fopen("output_sig_imaginary.dat", "w");

	for (int i = 0; i < SIG_LEN; i++)
	{
		fprintf(outRe_fptr, "\n%f", Output_DFT_Real[i]);
	}

	for (int i= 0; i < SIG_LEN; i++)
	{
		fprintf(outIm_fptr, "\n%f", Output_DFT_Imaginary[i]);
	}

	fclose(outRe_fptr);
	fclose(outIm_fptr);

	// IDFT algorithm.
	inversedft_algortihm(&InputSignal_f32_1kHz_15kHz[0],
				 &Output_DFT_Real[0],
				 &Output_DFT_Imaginary[0],
				 &Output_IDFT_Sig[0],
				 SIG_LEN);
	FILE *outIDFT_fptr;
	outIDFT_fptr = fopen("output_idft.dat", "w");

	for (int i = 0; i < SIG_LEN; i++)
	{
		fprintf(outIDFT_fptr, "\n%f", Output_IDFT_Sig[i]);
	}

	printf("\n\nMean of signal = %f\n\n", _mean_of_signal);
	printf("\n\nVariance of signal = %f\n", _var_of_signal);
	printf("\n\nSTD of signal = %f\n", _std_of_signal);

	return 0;
}

// Function to calculate signal mean.
double calc_signal_mean(double *sig_src_arr, int sig_length)
{
	// Caches mean of the signal.
	double _sig_mean = 0.0;

	// Add up all signal components
	// Divide the sum of signal components by the
	// to length of the signal array.
	for (int i = 0; i < sig_length; i++)
	{
		_sig_mean = _sig_mean + sig_src_arr[i];
	}
	_sig_mean = _sig_mean / (double) sig_length;

	return _sig_mean;
}

// Compute variance of a signal
double calc_signal_variance(double *sig_src_arr, double _sig_mean, int sig_length)
{
	double _sig_variance = 0.0; // Signal variance.

	// Computing the variance using variance formula.
	for (int j = 0; j < sig_length; j++)
	{
		_sig_variance = _sig_variance + pow((sig_src_arr[j] - _sig_mean), 2);
	}
	_sig_variance = _sig_variance / (sig_length - 1);

	return _sig_variance;
}

// Compute Standard deviation of a signal
// Theoretically, taking the square root of variance gives standard deviation.
double calc_standard_deviation(double _sig_variance)
{
	double _sig_std = sqrt(_sig_variance);
	return _sig_std;
}

// Convolution Algorithm (Kernel)
// Convolve two (2) signals (1000 Hz and 15000 Hz)
/*
 * @Inputs:
 *		_sig_src_arr
 * 		_sig_dest_arr
 *		_impulse_resp_arr
 * @Outputs: _sig_dest_arr
 */
void convolution(double *_sig_src_arr,
		 double *_sig_dest_arr,
		 double *_impulse_resp_arr,
		 int sig_src_len,
		 int imp_resp_len)
{
	int i, j;
	for (i = 0; i < sig_src_len+imp_resp_len; i++)
	{
		_sig_dest_arr[i] = 0;
	}

	for (i = 0; i < sig_src_len; i++)
	{
		for (j = 0; j < imp_resp_len; j++)
		{
			_sig_dest_arr[i+j] = _sig_dest_arr[i+j] + 
			_sig_src_arr[i]*_impulse_resp_arr[j];
		}
	}
}

// Running sum and first difference.
// Running sum algorithm can be used to smoothen the signal
// without using cutoff frequency - as a SMOOTHEN FILTER
// to take out high frequency signal without specifying 
// the cutoff frequency. For instance, the ECG signal
// like detecting the peak of the signal from noisy input
// signal.
// In summary, running sum can be used as a filter
// e.g., in moving average.
void running_sum(double *_sig_src_arr,
		 double *_sig_dest_arr,
		 int in_sig_len)
{
	for (int i = 0; i < in_sig_len; ++i)
	{
		_sig_dest_arr[i] = _sig_dest_arr[i-1] + _sig_src_arr[i];
	}
}